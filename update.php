<meta charset="UTF-8">
<meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0"/>
<?php
    require('db.php');
    if(isset($_POST['submit'])){
        // ตัวแปรสำหรับส่งรหัสฟอร์ม
        $b_id = $conn->real_escape_string($_POST['keyid']);
        //ในส่วนนี้จะเป็นชุดคำสั่งสำหรับอัพโหลดรูปหรือลบรูป
        $new_image = $_POST['data_file'];
        if($_FILES['file']['name']!= ''){
            $temp = explode('.',$_FILES['file']['name']); 
            $new_image = round(microtime(true)*9999).'.'.end($temp);
            $uploade  = $image_path.$new_image;

            if ( move_uploaded_file($_FILES['file']['tmp_name'], $uploade) ){
                $check_id = "SELECT * FROM `product` WHERE b_id = '".$b_id."' "; ///เรียกชื่อรูปออกมาทั้งหมดที่มี
                $results   = $conn->query($check_id)or die ('พัง');
                while($row = $results->fetch_array()){
                    $image = $row['image'];
                    @unlink($image_path.$image); //วนลูปเพื่อลบรูป
                }
            }else{
                echo '<script> alert("ไม่สามารถอัพโหลดรูปภาพใหม่ได้ โปรดลองอีกครั้ง")</script>'; 
                 header('Refresh:0; url=index.php'); 
            }
        }

        //ในส่วนอัพเดทข้อมูล
        //-------------------------------------------------------------//
        // ประการตัวแปรสำหรับเก็บค่าที่ได้รับมาจากฟอร์ม                        //
        $namebock = $conn->real_escape_string($_POST['namebock']);
        $author_name = $conn->real_escape_string($_POST['author_name']);
        $pric = $conn->real_escape_string($_POST['prict']);
        $details = $conn->real_escape_string($_POST['details']);
        //ตัวแปร frprice ใช้ number_format จาก pric เพื่อทำให้มีทศนิยมและตัว, เช่น 3,200.00 เป็นต้น
        $frprice = number_format($pric,2,'.',',');
        // คำสั่ง sql ในการอัพเดทข้อมูล
            $sql = "UPDATE `product` SET 
            `name` ='".$namebock."' ,
            `author_name` ='".$author_name."', 
            `details`='".$details."',
            `image`  = '".$new_image."',
            `price`  = '".$frprice."'
            WHERE `b_id` = '".$b_id."' ";

       $result = $conn->query($sql);
        if($result){
            echo "<script type='text/javascript'>";
            echo "alert('บันทึกข้อมูลสำเร็จ');";
            echo "window.location = 'index.php'; ";
            echo "</script>";
        }else{
            echo "<script type='text/javascript'>";
            echo "alert('บันทึกข้อ ไม่มูลสำเร็จ');";
            echo "window.location = 'index.php'; ";
            echo "</script>";
        }


    }

?>