<?php 
   require('db.php');
   $sql = "SELECT * FROM `product`";
   $result = $conn->query($sql);
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>ข้อมูลรายการสินค้า</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
      <link href="https://fonts.googleapis.com/css?family=Thasadith&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
      <style>
        body{
          background:url('assets/img/bg.jpg');
          font-family: 'Thasadith', sans-serif;
          font-size:16px;
        }
        h1{
          font-size:24px;
        }
      </style>
   </head>
   <body>
      <h1 class="mt-4 text-center text-dark">ข้อมูลรายการสินค้า</h1>
      <hr>
      <div class="container mt-4"> 
         <div class="row">
            <div class="col col-sm-12">
               <div class="jumbotron jumbotron-fluid">
               <div class="container">
                  <h1 class="display-4">ข้อมูลรายการสินค้า</h1>
                  <p class="lead">นี้คือตัวอย่าง Workshop การ insert update delete   โดยใช้ภาษา PHP ในการพัฒนา</p>
               </div>
               </div>
            </div>
            <div class="col col-sm-12 mt-2">
            
               <form action="mulitidelete.php" method="post">
                  <table id="example" class="table table-bordered" cellspacing="0">
                     <!--ส่วนหัว-->
                     <thead>
                        <tr>
                           <th colspan="8"  class='text-center'>
                              <a href="form.php" class="btn btn-sm btn-primary text-white">เพิ่มข้อมูล</a>
                              <input   type="submit" name="submit"  class="btn btn-sm btn-success" value=" - ลบข้อมูล "></td>
                           </th>
                        </tr>
                        <tr class="bg-danger text-white">
                           <th>ลำดับ</th>
                           <th width="200px">ชื่อหนังสือ</th>
                           <th width="100px">ชื่อผู้แต่ง</th>
                           <th>รายละเอียดสินค้า</th>
                           <th>รูปภาพ</th>
                           <th>ราคาสินค้า</th>
                           <th>จัดการ</th>

                           <th><input type="checkbox" id="checkAl">  All</th>
                        </tr>
                     </thead>
                     <!-- ส่วนท้าย -->   
                     <tfoot>
                        <tr  class="bg-danger text-white">
                           <th>ลำดับ</th>
                           <th>ชื่อหนังสือ</th>
                           <th>ชื่อผู้แต่ง</th>
                           <th>รายละเอียดสินค้า</th>
                           <th>รูปภาพ</th>
                           <th>ราคาสินค้า</th>
                           <th>จัดการ</th>
                           <th> All</th>
                        </tr>
                     </tfoot>
                     <!--ส่วนเนื้อหา -->
                     <tbody>
                        <?php
                           $num = 0;
                           while($row = $result->fetch_assoc()){
                               $num++
                           ?>
                        <tr  class="text-dark">
                           <td align="center"><?php echo $num;?></td>
                           <td><?php echo $row['name']?></td>
                           <td><?php echo $row['author_name'];?></td>
                           <td><?php echo $row['details'];?></td>
                           <td>
                              <?php if($row['image']!=''){?>
                               <img src="<?php echo $image_path.$row['image'];?>" width='100px' heigth="100px">
                              <?php }else{?>
                               <img src="assets/img/all.png" width='100px' heigth="100px">
                              <?php }?>
                           </td>
                           <td class="text-center">
                              <?php echo $row['price'];?>
                           </td>
                           <td  class='text-center'>
                              <!-- <a href="#" class="mt-2 btn btn-sm bg-warning"onclick="editItem(<?php echo $row['b_id']; ?>);"><i class="fa fa-edit"></i></a>
                              <a href="#" class="mt-2 btn btn-sm btn-danger"onclick="deleteItem(<?php echo $row['b_id']; ?>);"><i class="fa fa-trash"></i></a> -->
                              <a class="mt-2 btn btn-sm bg-warning" href="edit.php?b_id=<?php echo $row['b_id'];?>"><i class="fa fa-edit"></i></a>
                              <a class="mt-2 btn btn-sm btn-danger" href="delete.php?b_id=<?php echo $row['b_id'];?>"onClick="javascript:return confirm('คุณต้องการลบข้อมูลใช่หรือไม่');"><i class="fa fa-trash"></i></a>
                           </td>
                           <td class="text-center"><input type="checkbox" class="mt-4"id="checkItem" name="check[]" value="<?php echo $row["b_id"]; ?>"></td>
                        </tr>
                        <?php }?>
                     </tbody>
                  </table>
               </form>
            </div>
         </div>
      </div>
      <br><br>
      <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
      <script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript"src="main.js"></script>
   </body>
</html>