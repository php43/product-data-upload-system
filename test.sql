-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 02 ม.ค. 2020 เมื่อ 09:36 AM
-- เวอร์ชันของเซิร์ฟเวอร์: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- โครงสร้างตาราง `autoid`
--

CREATE TABLE `autoid` (
  `id` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- dump ตาราง `autoid`
--

INSERT INTO `autoid` (`id`) VALUES
('STD0001'),
('STD0002'),
('STD0003'),
('STD0004'),
('STD0005'),
('STD0006'),
('STD0007'),
('STD0008'),
('STD0009'),
('STD0010'),
('STD0011'),
('STD0012'),
('STD0013'),
('STD0014'),
('STD0015'),
('STD0016'),
('STD0017'),
('STD0018'),
('STD0019'),
('STD0020'),
('STD0021'),
('STD0022'),
('STD0023'),
('STD0024'),
('STD0025'),
('STD0026'),
('STD0027'),
('STD0028'),
('STD0029'),
('STD0030'),
('STD0031'),
('STD0032'),
('STD0033'),
('STD0034'),
('STD0035'),
('STD0036'),
('STD0037'),
('STD0038'),
('STD0039'),
('STD0040'),
('STD0041'),
('STD0042'),
('STD0043'),
('STD0044'),
('STD0045'),
('STD0046'),
('STD0047'),
('STD0048'),
('STD0049'),
('STD0050'),
('STD0051'),
('STD0052'),
('STD0053'),
('STD0054'),
('STD0055'),
('STD0056'),
('STD0057'),
('STD0058'),
('STD0059'),
('STD0060'),
('STD0061'),
('STD0062'),
('STD0063'),
('STD0064'),
('STD0065'),
('STD0066'),
('STD0067'),
('STD0068'),
('STD0069'),
('STD0070'),
('STD0071'),
('STD0072'),
('STD0073'),
('STD0074'),
('STD0075'),
('STD0076'),
('STD0077'),
('STD0078'),
('STD0079'),
('STD0080'),
('STD0081'),
('STD0082'),
('STD0083'),
('STD0084'),
('STD0085'),
('STD0086'),
('STD0087'),
('STD0088'),
('STD0089'),
('STD0090'),
('STD0091'),
('STD0092'),
('STD0093'),
('STD0094'),
('STD0095'),
('STD0096'),
('STD0097'),
('STD0098'),
('STD0099'),
('STD0100'),
('STD0101'),
('STD0102'),
('STD0103'),
('STD0104'),
('STD0105'),
('STD0106'),
('STD0107'),
('STD0108'),
('STD0109'),
('STD0110'),
('STD0111'),
('STD0112'),
('STD0113'),
('STD0114'),
('STD0115'),
('STD0116'),
('STD0117'),
('STD0118'),
('STD0119'),
('STD0120'),
('STD0121'),
('STD0122'),
('STD0123'),
('STD0124'),
('STD0125'),
('STD0126'),
('STD0127'),
('STD0128'),
('STD0129'),
('STD0130'),
('STD0131'),
('STD0132'),
('STD0133'),
('STD0134'),
('STD0135'),
('STD0136'),
('STD0137'),
('STD0138'),
('STD0139'),
('STD0140'),
('STD0141'),
('STD0142'),
('STD0143'),
('STD0144'),
('STD0145'),
('STD0146'),
('STD0147'),
('STD0148'),
('STD0149'),
('STD0150'),
('STD0151'),
('STD0152'),
('STD0153'),
('STD0154'),
('STD0155'),
('STD0156'),
('STD0157'),
('STD0158'),
('STD0159'),
('STD0160'),
('STD0161'),
('STD0162'),
('STD0163'),
('STD0164'),
('STD0165'),
('STD0166'),
('STD0167'),
('STD0168'),
('STD0169'),
('STD0170'),
('STD0171'),
('STD0172'),
('STD0173'),
('STD0174'),
('STD0175'),
('STD0176'),
('STD0177'),
('STD0178'),
('STD0179'),
('STD0180'),
('STD0181'),
('STD0182'),
('STD0183'),
('STD0184'),
('STD0185'),
('STD0186'),
('STD0187'),
('STD0188'),
('STD0189'),
('STD0190'),
('STD0191'),
('STD0192'),
('STD0193'),
('STD0194'),
('STD0195'),
('STD0196'),
('STD0197'),
('STD0198'),
('STD0199'),
('STD0200'),
('STD0201'),
('STD0202'),
('STD0203'),
('STD0204'),
('STD0205'),
('STD0206'),
('STD0207'),
('STD0208'),
('STD0209'),
('STD0210'),
('STD0211'),
('STD0212'),
('STD0213'),
('STD0214'),
('STD0215'),
('STD0216'),
('STD0217'),
('STD0218'),
('STD0219'),
('STD0220'),
('STD0221'),
('STD0222'),
('STD0223'),
('STD0224'),
('STD0225'),
('STD0226'),
('STD0227'),
('STD0228'),
('STD0229'),
('STD0230'),
('STD0231'),
('STD0232'),
('STD0233'),
('STD0234'),
('STD0235');

-- --------------------------------------------------------

--
-- โครงสร้างตาราง `custome`
--

CREATE TABLE `custome` (
  `customerid` varchar(14) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `countrycode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- dump ตาราง `custome`
--

INSERT INTO `custome` (`customerid`, `name`, `email`, `countrycode`) VALUES
('CST0001', '1', '1', 1);

-- --------------------------------------------------------

--
-- โครงสร้างตาราง `product`
--

CREATE TABLE `product` (
  `b_id` varchar(14) NOT NULL,
  `name` varchar(255) NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='products';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `autoid`
--
ALTER TABLE `autoid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custome`
--
ALTER TABLE `custome`
  ADD PRIMARY KEY (`customerid`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`b_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
